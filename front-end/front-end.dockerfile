FROM golang:1.21.5-alpine as builder

RUN mkdir /app

COPY . /app

WORKDIR /app

RUN CGO_ENABLED=0 go build -o frontApp ./cmd/web
RUN chmod +x /app/frontApp

FROM alpine:latest

RUN mkdir /app

COPY --from=builder /app/frontApp /app

WORKDIR /app

CMD ["./frontApp"]