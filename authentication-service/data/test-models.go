package data

import (
	"database/sql"
	"time"
)

type PostgresTestRepository struct {
	Conn *sql.DB
}

func NewPostgresTestRepository(conn *sql.DB) *PostgresTestRepository {
	return &PostgresTestRepository{
		Conn: conn,
	}
}

// Implement all methods of Repository interface
func (repo *PostgresTestRepository) GetAll() ([]*User, error) {
	users := []*User{}
	return users, nil
}

func (repo *PostgresTestRepository) GetByEmail(email string) (*User, error) {
	user := &User{
		ID:        1,
		FirstName: "First",
		LastName:  "Last",
		Email:     "me@here.com",
		Password:  "password",
		Active:    1,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	return user, nil
}

func (repo *PostgresTestRepository) GetOne(id int) (*User, error) {
	user := &User{
		ID:        1,
		FirstName: "First",
		LastName:  "Last",
		Email:     "me@here.com",
		Password:  "password",
		Active:    1,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	return user, nil
}

func (repo *PostgresTestRepository) Insert(user User) (int, error) {
	return 2, nil
}

func (repo *PostgresTestRepository) Update(user User) error {
	return nil
}

func (repo *PostgresTestRepository) DeleteByID(id int) error {
	return nil
}

func (repo *PostgresTestRepository) ResetPassword(password string, user User) error {
	return nil
}

func (repo *PostgresTestRepository) PasswordMatches(password string, user User) (bool, error) {
	return true, nil
}
