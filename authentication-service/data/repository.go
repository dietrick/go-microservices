package data

type Repository interface {
	GetAll() ([]*User, error)
	GetByEmail(email string) (*User, error)
	GetOne(id int) (*User, error)
	Insert(user User) (int, error)
	Update(user User) error
	DeleteByID(id int) error
	ResetPassword(password string, user User) error
	PasswordMatches(password string, user User) (bool, error)
}
